'use strict';
const dynamoDb = require("./config/dynamoDb");
const { sendResponse } = require("./functions/index");
const { v4: uuidv4 } = require('uuid');



module.exports.get = async event => {
    try {

        const params = {
            TableName: process.env.DYNAMO_TABLE_NAME,
        }
        const numbers = await dynamoDb.scan(params).promise();
        return sendResponse(200, { items: numbers.Items });
    } catch (e) {
        return sendResponse(500, { message: "Could not get the numbers list" });
    }
};


module.exports.upsert = async event => {
    try {
        const body = JSON.parse(event.body);

        const { number_value, id } = body
        if (id) {
            const params = {
                TableName: process.env.DYNAMO_TABLE_NAME,
                Key: {
                    id
                },
                ExpressionAttributeValues: {
                    ":number_value": number_value
                },
                UpdateExpression: "SET number_value = :number_value",
                ReturnValues: "ALL_NEW"
            };

            const data = await dynamoDb.update(params).promise();
            if (data.Attributes) {
                return sendResponse(200, data.Attributes);
            } else {
                return sendResponse(404, { message: "Updated post data not found" });
            }

        } else {
            const { number_value } = body;
            const id = uuidv4();
            const TableName = process.env.DYNAMO_TABLE_NAME;
            const params = {
                TableName,
                Item: {
                    id,
                    number_value
                },
                ConditionExpression: "attribute_not_exists(id)"
            };
            await dynamoDb.put(params).promise();
            return sendResponse(200, { message: 'number create' })
        }

    } catch (e) {
        return sendResponse(500, { message: "Could not upserr this number" });
    }
};


module.exports.add = async event => {
    try {
        const body = JSON.parse(event.body);
        const { number_value } = body
        return sendResponse(200, { data: { value: number_value + 1 } })

    } catch (e) {
        return sendResponse(500, { message: "operation invalid" });
    }
}

module.exports.subtract = async event => {
    try {
        const body = JSON.parse(event.body);
        const { number_value } = body
        return sendResponse(200, { data: { value: number_value - 2 } })

    } catch (e) {
        return sendResponse(500, { message: "operation invalid" });
    }
}

module.exports.divide = async event => {
    try {
        const body = JSON.parse(event.body);
        const { number_value } = body
        return sendResponse(200, { data: { value: number_value / 3 } })

    } catch (e) {
        return sendResponse(500, { message: "operation invalid" });
    }
}

module.exports.reset = async event => {
    try {
        return sendResponse(200, { data: { value: 0 } })

    } catch (e) {
        return sendResponse(500, { message: "operation invalid" });
    }

}